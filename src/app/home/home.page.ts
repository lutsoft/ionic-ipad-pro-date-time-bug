import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  startDate: string = "2019-08-27";
  startTime: string = "08:00";
  text: string = "text";
  
  constructor() {}

}
